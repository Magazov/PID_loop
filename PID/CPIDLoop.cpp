/*
 * PIDLoop.cpp
 *
 *  Created on: 10 Jun 2017
 *      Author: Vavat
 */

#include "CPIDLoop.h"
#include <limits>

/* Default PID parameters.
 * These should be close enough to make control fast, but stable.
 * Tuning PID is a skill not easily attained, but for reasonably stable
 * plant it should not be difficult to tune just by trial and error.
 * Normally for slow moving processes differential is zero.
 */
#define DEFAULT_P   1
#define DEFAULT_I   0
#define DEFAULT_D   0

CPIDLoop::Setup_T::Setup_T() :
        kP(DEFAULT_P), kI(DEFAULT_I), kD(DEFAULT_D), minOutput(
                -std::numeric_limits<float>::max()), maxOutput(
                std::numeric_limits<float>::max()), minIntegral(
                -std::numeric_limits<float>::max()), maxIntegral(
                std::numeric_limits<float>::max()), minDifferential(
                -std::numeric_limits<float>::max()), maxDifferential(
                std::numeric_limits<float>::max()) {
}

CPIDLoop::CPIDLoop() :
        m_Setup(), m_OldTarget(0), m_OldError(0), m_PContribution(0), m_IContribution(
                0), m_DContribution(0) {

}

void CPIDLoop::Reset() {
    m_OldTarget = 0;
    m_IContribution = 0;
}

void CPIDLoop::ResetParam() {
    m_Setup.kP = DEFAULT_P;
    m_Setup.kI = DEFAULT_I;
    m_Setup.kD = DEFAULT_D;
}

float CPIDLoop::Run(float target, float actual) {
    float error = target - actual;
    float output = Run(target, actual, error - m_OldError);
    m_OldError = error;
    return output;
}

float CPIDLoop::Run(float target, float actual, float deltaError) {
    float error = target - actual;
    float output = 0;

    if (!(m_Setup.kP == 0 && m_Setup.kI == 0 && m_Setup.kD == 0)) {
        /* Proportional term
         * Pterm = error * kP
         */
        m_PContribution = error * m_Setup.kP;

        /* Integral term
         * Iterm = Iterm + Error * kI
         * Integral term has "memory", i.e. error is accumulated over time.
         * As such care should be taken to ensure integral accumulator does not
         * end up with silly values, which then take time to unwind. Various checks
         * are performed during calculations to prevent this from happening.
         */
        float oldIContribution = m_IContribution;
        m_IContribution += error * m_Setup.kI;
        if (m_IContribution > m_Setup.maxIntegral) {
            m_IContribution = m_Setup.maxIntegral;
        }
        if (m_IContribution < m_Setup.minIntegral) {
            m_IContribution = m_Setup.minIntegral;
        }

        /* Differential term
         * Dterm = ((target - actual) - oldError) * kD
         */
        m_DContribution = deltaError * m_Setup.kD;
        if (m_DContribution > m_Setup.maxDifferential) {
            m_DContribution = m_Setup.maxDifferential;
        }
        if (m_DContribution < m_Setup.minDifferential) {
            m_DContribution = m_Setup.minDifferential;
        }

        /* Calculate output and perform range checking.
         * Integral term control is implemented to prevent integral windup.
         */
        output = m_PContribution + m_IContribution + m_DContribution;
        if (output > m_Setup.maxOutput) {
            output = m_Setup.maxOutput;
            /* integral windup prevention */
            if (m_IContribution > oldIContribution) {
                m_IContribution = oldIContribution;
            }
        } else if (output < m_Setup.minOutput) {
            output = m_Setup.minOutput;
            /* integral windup prevention */
            if (m_IContribution < oldIContribution) {
                m_IContribution = oldIContribution;
            }
        }
    }

    return output;
}
