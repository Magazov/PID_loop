/*
 * PIDLoop.h
 *
 *  Created on: 10 Jun 2017
 *      Author: Vavat
 */

#ifndef CPIDLOOP_H_
#define CPIDLOOP_H_

class CPIDLoop {
public:
    CPIDLoop();

    /* @brief:  Execute PID cycle
     * @arg:    target - value of variable PID should try to achieve by modifying output
     * @arg:    actual - current value of variable
     * @ret:    new value for output
     * @note:   loop execution will update integral accumulator.
     */
    float Run(float target, float actual);

    /* @brief:  Execute PID cycle
     * @arg:    target - value of variable PID should try to achieve by modifying output
     * @arg:    actual - current value of variable
     * @arg:    deltaError - rate of change of error. Used for Dterm calculation.
     * @ret:    new value for output
     * @note:   loop execution will update integral accumulator.
     */
    float Run(float target, float actual, float deltaError);

    /* @brief:  Reset PID loop to original state.
     *          Reset will not affect PID coefficients.
     */
    void Reset();

    /* @brief   Reset PID parameters to default values */
    void ResetParam();

    /* @brief:  Return proportional term of PID loop.
     * @ret:    Current P term.
     */
    float GetPterm() const {
        return m_Setup.kP;
    }

    /* @brief:  Set proportional term of PID loop.
     * @arg:    New P term.
     */
    void SetPterm(float term) {
        m_Setup.kP = term;
    }

    /* @brief:  Return integral term of PID loop.
     * @ret:    Current I term.
     */
    float GetIterm() const {
        return m_Setup.kI;
    }

    /* @brief:  Set integral term of PID loop.
     * @arg:    New I term.
     */
    void SetIterm(float term) {
        m_Setup.kI = term;
    }

    /* @brief:  Return differential term of PID loop.
     * @ret:    Current D term.
     */
    float GetDterm() const {
        return m_Setup.kD;
    }

    /* @brief:  Set differential term of PID loop.
     * @arg:    New D term.
     */
    void SetDterm(float term) {
        m_Setup.kP = term;
    }

    /* @brief:  Return maximum output of PID loop.
     * @ret:    Maximum output.
     */
    float GetMaxOut() const {
        return m_Setup.maxOutput;
    }

    /* @brief:  Set maximum output of PID loop.
     * @arg:    Maximum output.
     */
    void SetMaxOut(float output) {
        m_Setup.maxOutput = output;
    }

    /* @brief:  Return minimum output of PID loop.
     * @ret:    Minimum output.
     */
    float GetMinOut() const {
        return m_Setup.minOutput;
    }

    /* @brief:  Set minimum output of PID loop.
     * @arg:    Minimum output.
     */
    void SetMinOut(float output) {
        m_Setup.minOutput = output;
    }

    /* @brief:  Return maximum integral of PID loop.
     * @ret:    Maximum output.
     */
    float GetMaxIntegral() const {
        return m_Setup.maxIntegral;
    }

    /* @brief:  Set maximum integral of PID loop.
     * @arg:    Maximum integral.
     */
    void SetMaxIntegral(float integral) {
        m_Setup.maxIntegral = integral;
    }

    /* @brief:  Return minimum integral of PID loop.
     * @ret:    Minimum integral.
     */
    float GetMinIntegral() const {
        return m_Setup.minIntegral;
    }

    /* @brief:  Set minimum integral of PID loop.
     * @arg:    Minimum integral.
     */
    void SetMinIntegral(float integral) {
        m_Setup.minIntegral = integral;
    }

    /* @brief:  Return maximum differential of PID loop.
     * @ret:    Maximum differential.
     */
    float GetMaxDifferential() const {
        return m_Setup.maxDifferential;
    }

    /* @brief:  Set maximum differential of PID loop.
     * @arg:    Maximum differential.
     */
    void SetMaxDifferential(float differential) {
        m_Setup.maxDifferential = differential;
    }

    /* @brief:  Return minimum differential of PID loop.
     * @ret:    Minimum differential.
     */
    float GetMinDifferential() const {
        return m_Setup.minDifferential;
    }

    /* @brief:  Set minimum differential of PID loop.
     * @arg:    Minimum differential.
     */
    void SetMinDifferential(float differential) {
        m_Setup.minDifferential = differential;
    }

    /* @brief:  Return current proportional contribution to PID loop output.
     * @ret:    Proportional term contribution.
     */
    float GetPContribution() const {
        return m_PContribution;
    }

    /* @brief:  Return current integral contribution to PID loop output.
     * @ret:    Integral term contribution.
     */
    float GetIContribution() const {
        return m_IContribution;
    }

    /* @brief:  Return current differential contribution to PID loop output.
     * @ret:    Differential term contribution.
     */
    float GetDContribution() const {
        return m_DContribution;
    }

    /* @brief:  Set integral accumulator to PID loop output.
     * @arg:    Integral term contribution.
     */
    void SetIContribution(float iContribution) {
        m_IContribution = iContribution;
    }

    struct Setup_T {
        Setup_T();

        float kP;
        float kI;
        float kD;

        float minOutput;
        float maxOutput;

        float minIntegral;
        float maxIntegral;

        float minDifferential;
        float maxDifferential;
    };

    /* @brief:  Setup the loop using setup structure
     * @arg:    Setup_T structure containing all of settings.
     */
    void SetupLoop(const Setup_T & setup) {
        m_Setup = setup;
    }

    /* @brief:  Get the loop setup as a structure
     * @ret:    Setup_T structure pointer.
     */
    const Setup_T & GetSetup() {
        return m_Setup;
    }

private:

    Setup_T m_Setup;

    float m_OldTarget;
    float m_OldError;

    float m_PContribution;
    float m_IContribution;
    float m_DContribution;
};

#endif /* CPIDLOOP_H_ */
