# PID loop

This is an implementation of PID loop that served me well for all projects I've
done to date. This is a simpler non-time aware version. The benefit is that
there is no need to hook it to a timer. Disadvantage is that PID loop should be
run at reasonably regular intervals to minimise timing jitter induced noise in
the output.